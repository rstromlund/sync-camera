#!/usr/bin/env bash
set -x

typeset CURRENT_MONTH=$(date +"%Y%m") && \
	typeset LAST_MONTH=$(date --date="${CURRENT_MONTH}01 yesterday" +"%Y%m") && \
	mkdir -p photos
typeset returnCode=${?}

shopt -s extglob

ln ~/Downloads/camera-sg5/+(${LAST_MONTH}*|${CURRENT_MONTH}*).jpg photos/
# On my machine, `ln` returns 1 for file exists; ignore that "error" code.
(( ${?} <= 1 )) || (( returnCode += 2 ))

rm -f -v photos/!(${LAST_MONTH}*|${CURRENT_MONTH}*).jpg
# Same for `rm`
(( ${?} <= 1 )) || (( returnCode += 2 ))

chmod 644 photos/*.jpg
(( returnCode += ${?} ))

time rsync --delete --archive --update -v photos/ stromlund@stromlunds.net:/var/www/html/webdav/app/camera/
(( returnCode += ${?} ))

exit ${returnCode}
